function updateAccounts() {

    $('.edit-account').remove();
    
    Account.find({}).sort({ store: -1, email: -1 }).exec(function (err, docs) {
        $(docs).each(function(){
            var html =  '<tr class="hover press primary edit edit-account" data-open="account"><td>' +
                        '<span class="left"><h5>'+ this.email +'</h5></span>' +
                        '<span class="right"><label>' +this.store +'</label></span>' +
                        '</td></tr>'
            $('#account-list').prepend(html)
        })
    })
    
}

$(function(){

    $('body').on('change', '[name=no-login]', function(e){

        if ( $(e.target).is(':checked') ) $('[name=password], [name=show]').attr('disabled','disabled')
        else $('[name=password], [name=show]').removeAttr('disabled')
    })

    $('body').on('change', '[name=shipping-is-billing]', function(e){

        if ( $(e.target).is(':checked') )  $('.shipping-fields').css('max-height','0')
        else $('.shipping-fields').css('max-height','100px')
    })

    $('body').on('click', '#account [value=cancel]', function(e){
        $('.shipping-fields').css('max-height','0')
        $('#account form')[0].reset()
    });

    $('body').on('formvalid.zf.abide', '#account form',  function(e){

        var record = {}
        $.each( $(e.target).serializeArray(), function(){ if (!_.isEmpty(this)) record[this.name] = this.value })

        Account.insert(record, function (err) {
            updateAccounts()
        })

        return false
    })

    updateAccounts()
})
