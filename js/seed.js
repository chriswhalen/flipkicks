Store.insert([
{
        'name': 'adidas',
        'title': 'Adidas',
        'urls': {
            '_': 'https://www.adidas.com/',
            'search': 'us/{#}',
            'cart': 'on/demandware.store/Sites-adidas-US-Site/en_US/Cart-Show',
        }    
},{
        'name': 'adidas-canada',
        'title': 'Adidas Canada',
        'urls': {
            '_': 'https://www.adidas.ca/',
            'search': 'en/{#}',
            'cart': 'on/demandware.store/Sites-adidas-CA-Site/en_CA/Cart-Show',
        }    
},{     
        'name': 'foot-locker',
        'title': 'Foot Locker',
        'urls': {
            '_': 'https://www.footlocker.com/',
            'search': '_-_/keyword-{#}',
            'cart': 'catalog/shoppingCart',
        }
},{     
        'name': 'foot-locker-canada',
        'title': 'Foot Locker Canada',
        'urls': {
            '_': 'https://www.footlocker.ca/en-CA/',
            'search': '_-_/keyword-{#}',
            'cart': 'catalog/shoppingCart',
        }
}
])
