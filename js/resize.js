$(function(){

    $(window).resize(function(){

        var left = 0, top = 0
        var xs = $( ".x-resizable" )
        var ys = $( ".y-resizable" )
        var x = (win.width - $(window).width()) / xs.length
        var y = (win.height - $(window).height()) / ys.length

        xs.each(function(i,e){
            $(this).width($(this).width() - x)
            if (i) {
                left += $(this).prev().width()
                $(this).css('left', left )
            }
        })

        ys.each(function(i,e){
            $(this).height($(this).height() - y)
            if (i) {
                top += $(this).prev().height()
                $(this).css('top', top )
            }
        })

        $('.parent-width').each(function(){ $(this).width($(this).parent().width()) })
        $('.parent-height').each(function(){ $(this).height($(this).parent().height()) })

        win = { width: $(window).width(), height: $(window).height() }
    })

    $( ".x-resizable" ).resizable({ handles: 'e, w' }).on('resize', function (e) {

        if (e.pageX < 60 || e.pageX > window.innerWidth-60 ) { return false }

        if ( resize == 'w' ) {
            $(this).prev().width( $(this).prev().width() + (e.pageX-coord.x) )
        }
        if ( resize == 'e' ) {
            $(this).next().width( $(this).next().width() + (coord.x-e.pageX) ).css('left', $(this).width() )
        }

        var siblingWidth = 0
        var siblingCount = 0

        $(this).parent().children(".x-resizable").each(function() {
            siblingWidth += $(this).width()
            siblingCount++
        })

        if ( siblingWidth !== $(this).parent().width() ) {
            $(this).parent().children(".x-resizable").each(function() {
                if ( $(this).width() < 60 ) return false
                var offset = ( $(this).parent().width() - siblingWidth ) / siblingCount
                $(this).width( $(this).width() + offset )
            })
        }

        coord.x = e.pageX
    })

    $( ".y-resizable" ).resizable({ handles: 'n, s' }).on('resize', function (e) {

        if (e.pageY < 60 || e.pageY > window.innerHeight-60 ) { return false }

        if ( resize == 's' ) {
            $(this).prev().height( $(this).prev().height() + (e.pageY-coord.y) )
        }
        if ( resize == 'n' ) {
            $(this).next().height( $(this).next().height() + (coord.y-e.pageY) ).css('top', $(this).height() )
        }

        var siblingHeight = 0
        var siblingCount = 0

        $(this).parent().children(".y-resizable").each(function() {
            siblingHeight += $(this).height()
            siblingCount++
        })

        if ( siblingHeight !== $(this).parent().height() ) {
            $(this).parent().children(".y-resizable").each(function() {
                if ( $(this).height() < 60 ) return false
                var offset = ( $(this).parent().height() - siblingHeight ) / siblingCount
                $(this).height( $(this).height() + offset )
            })
        }

        coord.y = e.pageY
    })

    $('.ui-resizable-n').on('mousedown', function(e) { resize = 'n'; coord.y = e.pageY })
    $('.ui-resizable-s').on('mousedown', function(e) { resize = 's'; coord.y = e.pageY })
    $('.ui-resizable-w').on('mousedown', function(e) { resize = 'w'; coord.x = e.pageX })
    $('.ui-resizable-e').on('mousedown', function(e) { resize = 'e'; coord.x = e.pageX })

    win = { width: $(window).width(), height: $(window).height() }

    setTimeout(function(){
        $('.parent-width').each(function(){ $(this).width($(this).parent().width()) })
        $('.parent-height').each(function(){ $(this).height($(this).parent().height()) })
        $(window).resize();
    }, 60)
})
