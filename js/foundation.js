Foundation.Abide.defaults.validators['checked'] =
    function($el,required,parent) {
        return ( $('[name='+$el.attr('data-checked')+']').is(':checked') || !!$el.val().length )
    };

Foundation.Abide.defaults.validators['not-checked'] =
    function($el,required,parent) {
        return ( !$('[name='+$el.attr('data-checked')+']').is(':checked') || !!$el.val().length )
    };
