resize = false
coord = { x: 0, y: 0 }
win = { width: $(window).width(), height: $(window).height() }

nedb = require('nedb')
Account = new nedb({ filename: 'db/accounts.json', autoload: true })
Store = new nedb({ filename: 'db/stores.json', autoload: true })
Task = new nedb({ filename: 'db/tasks.json', autoload: true })

require('./helpers')
require('./resize')
require('./accounts')
require('./tasks')
require('./foundation')

$(function(){

    $('body').on('click', 'label', function(e){

        $(e.target).parents('form').find('[name='+ $(e.target).attr('for') +']').click().focus()
    })

    $('select').select2().find(".select2-selection__rendered").sortable({
        containment: 'parent',
        start: function() { $(this).select2("onSortStart"); },
        update: function() { $(this).select2("onSortEnd"); }
    });

    $('.select2').width('100%')
    $(document).foundation()

    $('body').on('change', '[name=show]', function(e){

        var password = $(this).parents('form').find('[name=password]')
        if ( $(e.target).is(':checked') ) password.attr('type', 'text')
        else password.attr('type', 'password')
    })

    $('body').on('click', 'td.add', function(e){
    
        $('.form-buttons .add').show();
        $('.form-buttons .edit').hide();
    });

    $('body').on('click', 'td.edit', function(e){
    
        $('.form-buttons .edit').show();
        $('.form-buttons .add').hide();
    });


    $('body').on('click', '.select-all', function(e){

        var select = $(e.target).parents('form').find('[name='+ $(e.target).attr('for') +']')
        select.find('option').prop("selected","selected")
        select.trigger("change")
    });

    $('body').on('click', '.select-none', function(e){

        var select = $(e.target).parents('form').find('[name='+ $(e.target).attr('for') +']')

        select.val('')
        select.trigger("change")
    });

})
