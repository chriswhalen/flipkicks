$(function(){

    $('body').on('click', '#add-task:not(.active)', function(e){

        $('#add-task').removeClass('success').addClass('active');
        return false
    })

    $('body').on('click', '#add-task.active [value=cancel]', function(e){

        $('#add-task form')[0].reset()
        $('#add-task').addClass('success').removeClass('active')
        return false
    })

    $('body').on('click', '#add-task.active [value=submit]', function(e){

        $('#add-task').addClass('success').removeClass('active')
        return false
    })

    $('body').on('submit', '#add-task', function(e){ return false; })
    $('body').on('formvalid.zf.abide', '#add-task',  function(e){

        var record = {}
        $.each( $(e.target).serializeArray(), function(){ if (!_.isEmpty(this)) record[this.name] = this.value })

        Task.insert(record, function (err) {

            $('#add-task').before('<tr class="hover press primary edit-task"><td><h5>'+record.username+'</h5></td></tr>')
            $('#add-task form')[0].reset()
            $('#add-task').addClass('success').removeClass('active')
        })

        return false
    })
})
