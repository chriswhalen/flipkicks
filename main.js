const { app } = require('electron')
const path = require('path')
const window = require('electron-window')

app.on('ready', () => {

    const mainWindow = window.createWindow({
        width: 1280,
        height: 640,
        minWidth: 640,
        minHeight: 400,
        backgroundColor: '#12120C',
    })

    mainWindow.showUrl('index.html', {}, () => {})
})
